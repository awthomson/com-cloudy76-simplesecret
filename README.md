# SimpleSecret

This is a simple serverless webapp to securely share secrets with others

To access the hosted instance visit here: https://simplesecret.cloudy76.com

/infra - contains CloudFormation script to stand up AWS resources
/website - contains the React front-end component


Other tools you may find useful:
* YAJE - Yet Another JSON Editor (and validator!) https://yaje.cloudy76.com
* AWS Policy Generator - A quick way to search across all AWS actions https://aws-policy.cloudy76.com
