GETLAMBDA=$(aws lambda list-functions | jq -r .Functions[].FunctionArn | grep getSecret)
POSTLAMBDA=$(aws lambda list-functions | jq -r .Functions[].FunctionArn | grep createSecret)

cat << EOF > spec.yml
openapi: "3.0.1"
info:
  title: "SimpleSecret API v2"
  description: "Create and retreive secrets"
  version: "1.0.0"
servers:
- url: "https://b86c136vx5.execute-api.ap-southeast-2.amazonaws.com/{basePath}"
  variables:
    basePath:
      default: ""
paths:
  /{id}:
    get:
      responses:
        default:
          description: "Default response for GET /{id}"
      x-amazon-apigateway-integration:
        payloadFormatVersion: "2.0"
        type: "aws_proxy"
        httpMethod: "GET"
        uri: "${GETLAMBDA}"
        connectionType: "INTERNET"
  /:
    post:
      responses:
        default:
          description: "Default response for POST /"
      x-amazon-apigateway-integration:
        payloadFormatVersion: "2.0"
        type: "aws_proxy"
        httpMethod: "POST"
        uri: "arn:aws:apigateway:ap-southeast-2:lambda:path/2015-03-31/functions/arn:aws:lambda:ap-southeast-2:170004535943:function:simplesecret-createSecret/invocations"
        connectionType: "INTERNET"
x-amazon-apigateway-importexport-version: "1.0"
EOF
aws apigatewayv2 import-api --body "$(cat spec.yml)"

