GETLAMBDA=$(aws lambda list-functions | jq -r .Functions[].FunctionArn | grep getSecret)
POSTLAMBDA=$(aws lambda list-functions | jq -r .Functions[].FunctionArn | grep createSecret)

APIID=$(aws apigatewayv2 create-api --name alextest --protocol-type HTTP | jq -r .ApiId)

GETROUTE=$(aws apigatewayv2 create-route --api-id ${APIID} --route-key "GET /{id}" | jq -r .RouteId)
POSTROUTE=$(aws apigatewayv2 create-route --api-id ${APIID} --route-key "POST /" | jq -r .RouteId)

GETINT=$(aws apigatewayv2 create-integration --integration-type AWS_PROXY --api-id ${APIID} --integration-uri "${GETLAMBDA}" --payload-format-version "2.0" | jq -r .IntegrationId)
POSTINT=$(aws apigatewayv2 create-integration --integration-type AWS_PROXY --api-id ${APIID} --integration-uri "${POSTLAMBDA}" --payload-format-version "2.0" | jq -r .IntegrationId)

aws apigatewayv2 update-route --api-id ${APIID} --route-id ${GETROUTE} --target integrations/${GETINT}
aws apigatewayv2 update-route --api-id ${APIID} --route-id ${POSTROUTE} --target integrations/${POSTINT}

aws apigatewayv2 create-stage --api-id ${APIID} --stage-name prod
