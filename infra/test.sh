#!/bin/bash

API_ID=$(aws apigateway get-rest-apis | jq .items[0].id -r)
# API_EP=$(aws apigatewayv2 get-apis | jq .Items[0].ApiEndpoint -r)
AUTH_KEY="abc"

URL="https://${API_ID}.execute-api.ap-southeast-2.amazonaws.com/prod"
# echo "URL: ${URL}/prod"
echo ""

echo "Creating a secret..."
XUID=$(curl -k -s -X POST -H "Content-Type: application/json" -d '{"secret":"xxx","expire":"1hr"}'  ${URL}   | jq .uid -r)
curl -k -X POST -H "Content-Type: application/json" -d '{"secret":"xxx","expire":"1hr"}'  -H "Authorization: ${AUTH_KEY}" ${URL}
echo ""
echo ""

echo "Retrieving a secret..."
curl -s -k -H "Content-Type: application/json" ${URL}/${XUID} -H "Authorization: ${AUTH_KEY}"
echo ""
echo ""

echo "Retrieving a secret again..."
curl -s -k -w "%{http_code}" -H "Content-Type: application/json" ${URL}/${XUID} -H "Authorization: ${AUTH_KEY}"
echo ""
echo ""

exit 0

